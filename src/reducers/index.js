import { List, Map } from 'immutable';


const initialState = List([]);

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case 'ADD_COMMENT':
            return state.push(Map(action.payload));
        case 'DELETE_COMMENT':
            return state.filter((comment) => comment.get('id') !== action.payload.id);
        default:
            return state;
    }
};

export default reducer;
