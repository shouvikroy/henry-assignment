// Libs
import React from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";

// Styles
import "./index.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";

// app
import reducer from "./reducers";
import App from "./App";

const store = createStore(reducer);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
