import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addComment, deleteComment } from './actions';
import NewCommentForm from './components/NewCommentForm';
import CommentList from './components/CommentList';
import './App.css';

class App extends Component {
  render() {
    return <div className="container-fluid">
        <h1 className="text-center mb-5"> Henry Assignment </h1>
        <div className="row">
          <div className="col-md">
            <NewCommentForm onAdd={this.props.addComment} />
          </div>
          <div className="col-md">
            <CommentList comments={this.props.comments.toJS()} onDelete={this.props.deleteComment}/>
          </div>
        </div>
      </div>;
  }
}

App.propTypes = {
  addComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({ comments: state});

export default connect(mapStateToProps, { addComment, deleteComment })(App);

