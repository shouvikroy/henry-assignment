import React from 'react';
import PropTypes from 'prop-types';

const Comment = props => {
    return <li className="list-group-item d-flex justify-content-between align-items-baseline">
        <p>
          <span className="score text-secondary">{props.comment.score}</span>
          <span className="lead">{props.comment.text}</span>
        </p>
        <a onClick={() => props.onDelete(props.comment.id)}className="text-right text-danger delete">Delete</a>
      </li>;
}

Comment.propTypes = {
    comment: PropTypes.shape({
        id: PropTypes.number,
        text: PropTypes.string,
        score: PropTypes.number
    }),
    onDelete: PropTypes.func.isRequired
}

export default Comment;
