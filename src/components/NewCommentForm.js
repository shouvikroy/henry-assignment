import React, { Component } from "react";
import PropTypes from "prop-types";

class NewCommentForm extends Component {
  constructor(props) {
      super(props);
      this.state = {
          text: '',
          score: 1
      }
  }
  handleTextChange = (event) => {
      this.setState({
          text: event.target.value
      })
  }

  handleScoreChange = (event) => {
    this.setState({
        score: Number(event.target.value)
    });
  }

  handleSubmit = (event) => {
      event.preventDefault();
      const { text, score } = this.state;
      if (text.length > 0) {
          this.props.onAdd({
              text,
              score
          });
          this.setState({ text: '', score: 1 });
      }
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="comment">Comment</label>
          <input
            onChange={this.handleTextChange}
            value={this.state.text}
            type="text"
            className="form-control"
            id="comment"
            placeholder="What do you have to say?"
          />
        </div>
        <div className="form-group">
          <label htmlFor="score">Score</label>
          <select className="form-control" id="score" onChange={this.handleScoreChange} value={this.state.score}>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </div>
        <button type="submit" className="btn btn-primary mb-4">
          Add Comment
        </button>
      </form>
    );
  }
}

NewCommentForm.propTypes = {
  onAdd: PropTypes.func.isRequired
};

export default NewCommentForm;
