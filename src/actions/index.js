const generateId = () => Number(new Date())

export const addComment = ({ text, score} ) => ({
    type: 'ADD_COMMENT',
    payload: { 
        id: generateId(),
        text,
        score
    }
});


export const deleteComment = (id) => ({
    type: 'DELETE_COMMENT',
    payload: {
        id,
    }
});

