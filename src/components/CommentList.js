import React from 'react';
import PropTypes from 'prop-types';
import Comment from './Comment';

const CommentList = props => {
    const hasComments = props.comments.length !== 0;
    return hasComments ? (
        <div className="card">
        <div className="card-header">Comments added so far</div>
        <ul className="list-group list-group-flush">
          { props.comments.map((comment) => 
            <Comment
                key={comment.id}
                comment={comment}
                onDelete={props.onDelete}
            />
            )}
        </ul>
      </div>
    ) : <h6 className="text-center"> No comments to display </h6>
}

CommentList.propTypes = {
    comments: PropTypes.arrayOf(PropTypes.shape({
        text: PropTypes.string,
        score: PropTypes.number
    })),
    onDelete: PropTypes.func.isRequired
}

export default CommentList;
